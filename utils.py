
import tensorflow as tf

def print_layer_info(name, net):
    print(name, ': ', net.shape)

def print_total_parameters():
    total_parameters = 0
    for variable in tf.trainable_variables():
        shape = variable.get_shape()
        variable_parametes = 1
        for dim in shape:
            variable_parametes *= dim.value
        total_parameters += variable_parametes

    print('Total training params: {:.1f}M'.format(total_parameters / 1e6))

# def parse_function_train(example_proto):
#     features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
#                 "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
#     parsed_features = tf.parse_single_example(example_proto, features)

#     image_raw = tf.decode_raw(parsed_features["image"], tf.uint8)
#     image = tf.reshape(image_raw, [32, 32, 3])
#     reshaped_image = tf.cast(image, tf.float32)

#     distorted_image = tf.image.resize_image_with_crop_or_pad(reshaped_image, 40, 40)

#     # Randomly crop a [height, width] section of the image.
#     distorted_image = tf.random_crop(reshaped_image, [32, 32, 3])

#     # Randomly flip the image horizontally.
#     distorted_image = tf.image.random_flip_up_down(distorted_image)

#     # Subtract off the mean and divide by the variance of the pixels.
#     float_image = tf.image.per_image_standardization(distorted_image)

#     # Set the shapes of tensors.
#     float_image.set_shape([32, 32, 3])

#     return float_image, parsed_features["label"]

def parse_function_train(example_proto):
    features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
    parsed_features = tf.parse_single_example(example_proto, features)

    image_raw = tf.decode_raw(parsed_features["image"], tf.uint8)
    image = tf.reshape(image_raw, [32, 32, 3])
    reshaped_image = tf.cast(image, tf.float32)

    # distorted_image = tf.image.resize_image_with_crop_or_pad(reshaped_image, 40, 40)
    distorted_image = tf.image.resize_images(reshaped_image, [224,224])

    # # Randomly crop a [height, width] section of the image.
    # distorted_image = tf.random_crop(reshaped_image, [32, 32, 3])

    # Randomly flip the image horizontally.
    distorted_image = tf.image.random_flip_up_down(distorted_image)

    # Subtract off the mean and divide by the variance of the pixels.
    float_image = tf.image.per_image_standardization(distorted_image)

    # Set the shapes of tensors.
    float_image.set_shape([224, 224, 3])

    return float_image, parsed_features["label"]

def parse_function_test(example_proto):
    features = {"image": tf.FixedLenFeature((), tf.string, default_value=""),
                "label": tf.FixedLenFeature((), tf.int64, default_value=0)}
    parsed_features = tf.parse_single_example(example_proto, features)

    image_raw = tf.decode_raw(parsed_features["image"], tf.uint8)
    image = tf.reshape(image_raw, [32, 32, 3])
    reshaped_image = tf.cast(image, tf.float32)

    # Subtract off the mean and divide by the variance of the pixels.
    float_image = tf.image.per_image_standardization(reshaped_image)

    # Set the shapes of tensors.
    float_image.set_shape([32, 32, 3])

    return float_image, parsed_features["label"]