import tensorflow as tf
import numpy as np

import utils

class DQN():
    def __init__(self, flags, images, labels, network_name):
        self.flags = flags
        self.network_name = network_name
        self.is_training = tf.placeholder(tf.bool, name='is_training')
        self.lr = tf.placeholder(tf.float32, name='learning_rate')
        self.global_step = tf.train.get_or_create_global_step()

        self.build_model(images, labels)

    def build_model(self, images, labels):
        with tf.variable_scope(self.network_name):
            out = tf.layers.conv2d(inputs=images, filters=32,
                                   kernel_size=[8,8], strides=(4,4),
                                   padding='valid',
                                   kernel_initializer=tf.variance_scaling_initializer(),
                                   kernel_regularizer=tf.contrib.layers.l2_regularizer(self.flags.weight_decay),
                                   name='conv2d_1')
            out = tf.layers.batch_normalization(out, training=self.is_training, fused=True)
            out = tf.nn.relu(out)
            if self.flags.show_layer_info:
                utils.print_layer_info('conv2d_1', out)

            out = tf.layers.conv2d(out, filters=64, 
                                   kernel_size=[4,4], strides=(2,2), 
                                   padding='valid',
                                   kernel_initializer=tf.variance_scaling_initializer(),
                                   kernel_regularizer=tf.contrib.layers.l2_regularizer(self.flags.weight_decay),
                                   name='conv2d_2')
            out = tf.layers.batch_normalization(out, training=self.is_training, fused=True)
            out = tf.nn.relu(out)
            if self.flags.show_layer_info:
                utils.print_layer_info('conv2d_2', out)

            out = tf.layers.conv2d(out, filters=64, 
                                   kernel_size=[3,3], strides=(1, 1),
                                   padding='valid',
                                   kernel_initializer=tf.variance_scaling_initializer(),
                                   kernel_regularizer=tf.contrib.layers.l2_regularizer(self.flags.weight_decay),
                                   name='conv2d_3')
            out = tf.layers.batch_normalization(out, training=self.is_training, fused=True)
            out = tf.nn.relu(out)
            if self.flags.show_layer_info:
                utils.print_layer_info('conv2d_3', out)
                                   
            out = tf.layers.flatten(out)
            out = tf.layers.dense(out, 512, activation=tf.nn.relu, name='fc')
            if self.flags.show_layer_info:
                utils.print_layer_info('fc', out)

            out = tf.layers.dense(out, 10, name='output_layer')
            if self.flags.show_layer_info:
                utils.print_layer_info('output_layer', out)

        self.logits = out
        
        # losses
        self.loss = tf.losses.sparse_softmax_cross_entropy(labels, self.logits)
        self.total_loss = tf.losses.get_total_loss()

        # accuracy
        self.prediction = tf.argmax(tf.nn.softmax(self.logits), axis=1, output_type=tf.int32)
        correct_prediction = tf.equal(self.prediction, labels)
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    def create_train_op(self):
        # Build optimizer
        optimizer = tf.train.MomentumOptimizer(learning_rate=self.lr, 
                                               momentum=0.9,
                                               use_nesterov=True)

        # Build train operation
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_op = optimizer.minimize(loss=self.total_loss, global_step=self.global_step)

        return train_op
