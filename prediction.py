import tensorflow as tf
import numpy as np

# from plain18 import Plain
from dqn import DQN
# from resnet18 import Resnet

class Prediction():
    def __init__(self, width=120, height=120, weight_decay=0.001):
        self.width = width
        self.height =  height
        self.wd = weight_decay

        self.sess = tf.Session()
        self.restore()

    def get_arguments(self):
        class Arguments: pass
        args = Arguments()

        args.log_path = '../../train/terminal_pred/'
        args.dir_name = ''
        args.batch_size = 32
        args.weight_decay = self.wd
        args.network = 'dqn'
        args.show_layer_info = False

        args.dir_name += args.network
        args.dir_name += ('/batch_size_' + str(args.batch_size))
        args.dir_name += '/wd_' + str(args.weight_decay)
        args.logdir = args.log_path + args.dir_name + '/'
        args.check_point_file = args.logdir + 'model.ckpt'

        return args

    def restore(self):
        self.images_placeholder = tf.placeholder(tf.float32, shape=[None,self.width,self.height,1], name='images')
        self.labels_placeholder = tf.placeholder(tf.int32, shape=[None], name='labels')

        self.flags = self.get_arguments()
        self.network = DQN(self.flags, self.images_placeholder, self.labels_placeholder, 'plain')

        # Build saver graph
        self.saver = tf.train.Saver()
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir)
        if checkpoint_path is not None:
            self.saver.restore(self.sess, checkpoint_path)
            print('Network is restored')
        else:
            print('checkpoint_path i none')

    def predict(self, image):
        if len(image.shape) == 2:
            image = np.expand_dims(image, axis=2)

        result = self.sess.run(self.network.prediction, feed_dict={self.network.is_training: False,
                                                                   self.images_placeholder: [image]})
        
        if result.item() == 0:
            return False
        else:
            return True
