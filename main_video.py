import numpy as np
import cv2
import glob
import skvideo.io
import time

from rewardreader import RewardReader

cap = cv2.VideoCapture('sample_video.mov')
save_image = False
save_diff_image = True
# save_video = False
bar_scale = 0.667

# bar position and size
bar_pos_l = (158,92)
bar_pos_r = (1031,92)
bar_width = 730
bar_height = 22

# instance reward reader
reader_l = RewardReader(round(bar_height*bar_scale), round(bar_width*bar_scale))
reader_r = RewardReader(round(bar_height*bar_scale), round(bar_width*bar_scale))

index = 0
is_diff = False
font = cv2.FONT_HERSHEY_SIMPLEX
time_buf = []
frame_merged_prev = 0
w_prev_l = round(bar_width*bar_scale)
w_prev_r = round(bar_width*bar_scale)
reward_prev = 0

# if save_video:
#     video_width = int(730/2)
#     video_height = int(video_width/2)
#     # start the FFmpeg writing subprocess with following parameters
#     writer = skvideo.io.FFmpegWriter('result.mp4', 
#         inputdict={
#         "-r": "60"
#     })

while(cap.isOpened()):
    ret, frame = cap.read()
    frame_debug = np.zeros([29,480,3], dtype=np.uint8)

    # get left bar image
    frame_crop_l = frame[bar_pos_l[1]:bar_pos_l[1]+bar_height, bar_pos_l[0]:bar_pos_l[0]+bar_width]

    # get right bar image
    frame_crop_r = frame[bar_pos_r[1]:bar_pos_r[1]+bar_height, bar_pos_r[0]:bar_pos_r[0]+bar_width]
    frame_crop_r = np.fliplr(frame_crop_r)

    # resize bar image
    if bar_scale != 1.0:
        frame_crop_l = cv2.resize(frame_crop_l, None, fx=bar_scale, fy=bar_scale, interpolation=cv2.INTER_AREA)
        frame_crop_r = cv2.resize(frame_crop_r, None, fx=bar_scale, fy=bar_scale, interpolation=cv2.INTER_AREA)

    # resize frame for video recording
    frame_video = cv2.resize(frame, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_AREA)

    # get bound rectangle info and reward
    start_time = time.time()
    result_l = reader_l.get_bound_rect(frame_crop_l)
    result_r = reader_r.get_bound_rect(frame_crop_r)
    elapsed_time = time.time() - start_time
    time_buf.append(elapsed_time)
    if len(time_buf)==50:
        print('index: {}, proc. time: {:.3f} ms'.format(index+1, np.mean(time_buf)*1000))
        time_buf = []

    # left gauge
    frame_rect_l = np.copy(frame_crop_l)
    status, (x,y,w,h), reward = result_l
    debug_str = 'L: w=' + str(w) + ', r=' + str(reward)
    if status == 'get':
        # draw rectangle and show width info
        frame_rect_l = cv2.rectangle(frame_rect_l,(x,y),(x+w,y+h),(255,0,0),1)
    else:
        # cv2.putText(frame_rect_l,'Block',(400,15), font, 0.5,(0,0,255),2,cv2.LINE_AA)
        debug_str += ' (block)'
    cv2.putText(frame_debug, debug_str, (5, 20), font, 0.6, (255,255,255), 1, cv2.LINE_AA)
    if w != w_prev_l:
        is_diff = True
    w_prev_l = w

    # right gauge
    frame_rect_r = np.copy(frame_crop_r)
    status, (x,y,w,h), reward = result_r
    debug_str = 'R: w=' + str(w) + ', r=' + str(reward)
    if status == 'get':
        # draw rectangle and show width info
        frame_rect_r = cv2.rectangle(frame_rect_r,(x,y),(x+w,y+h),(255,0,0),1)
    else:
        # cv2.putText(frame_rect_r,'Block',(400,15), font, 0.5,(0,0,255),2,cv2.LINE_AA)
        debug_str += ' (block)'
    cv2.putText(frame_debug, debug_str, (245, 20), font, 0.6, (255,255,255), 1, cv2.LINE_AA)
    if w != w_prev_r:
        is_diff = True
    w_prev_r = w

    # merge left and right gauge image
    frame_merged_l = np.concatenate((frame_crop_l,frame_rect_l), axis=0)
    frame_merged_r = np.concatenate((frame_crop_r,frame_rect_r), axis=0)
    frame_merged = np.concatenate((frame_merged_l,frame_merged_r), axis=0)

    # resize and merge with frame_debug
    merged_height = round(480 * frame_merged.shape[0] / frame_merged.shape[1])
    frame_merged = cv2.resize(frame_merged, (480,merged_height), 0, 0, interpolation=cv2.INTER_AREA)
    frame_merged = np.concatenate((frame_merged,frame_debug), axis=0)
    
    cv2.imshow('frame_merged',frame_merged)
    cv2.imshow('original',frame_video)
    cv2.moveWindow('frame_merged', 500, 100)
    cv2.moveWindow('original', 500, 320)

    if save_image or (save_diff_image and is_diff):
        is_diff = False
        cv2.imwrite('temp/image_'+str(index).rjust(4,'0')+'_l.bmp', frame_crop_l)
        cv2.imwrite('temp/image_'+str(index).rjust(4,'0')+'_r.bmp', frame_crop_r)
        cv2.imwrite('temp/image_merged_'+str(index).rjust(4,'0')+'.png', frame_merged)
        # cv2.imwrite('temp/frame_video_'+str(count)+'.png', frame_video)

    # if reward_prev != reward and reward !=0:
    #     cv2.imwrite('image_merged_'+str(index-1).rjust(4,'0')+'.png', frame_merged_prev)
    #     cv2.imwrite('image_merged_'+str(index).rjust(4,'0')+'.png', frame_merged)

    # if save_video:
    #     video_frame = np.zeros([video_height, video_width, 3])
    #     video_frame[100:100+frame_merged.shape[0],:,:] = frame_merged
    #     writer.writeFrame(video_frame.astype(np.uint8))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    index += 1
    frame_merged_prev = frame_merged
    reward_prev = reward

# if save_video:
#     writer.close()

cap.release()
cv2.destroyAllWindows()