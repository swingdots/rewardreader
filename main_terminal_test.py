import numpy as np
import cv2

from prediction import Prediction

cap = cv2.VideoCapture('clip2.mov')
pred = Prediction()

while(cap.isOpened()):
    ret, frame = cap.read()

    frame_resized = cv2.resize(frame, (120,120), fx=0, fy=0, interpolation=cv2.INTER_AREA)
    frame_gray = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2GRAY)

    is_terminal = pred.predict(frame_gray)
    print(is_terminal)

    frame_disp = cv2.resize(frame, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_AREA)
    cv2.imshow('frame', frame_disp)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break