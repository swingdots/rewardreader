import numpy as np
import cv2
from collections import deque

class RewardReader():
    def __init__(self, height, width, is_image=False):
        self.width = width
        self.height = height
        self.img_size = (height, width)
        self.is_image = is_image
        self.prev_rect = (0, 0, width, height)
        self.thresh_black = 20      # threshold for balck and gray color
        self.thresh_red = 20        # threshold for red color
        self.count_no_contour = 0

        # minimum area of contour blob
        self.min_blob_area = 20/730 * self.width

        # width buffer
        self.w_hist = deque(maxlen=5)
        for i in range(5):
            self.w_hist.append(self.width)

    def get_bound_rect(self, image):
        # update red threshold
        self.update_thresh()

        w_prev = self.prev_rect[2]

        # convert to l*a*b image
        img_norm = image/255.
        img_norm = img_norm.astype(np.float32)
        img_lab = cv2.cvtColor(img_norm, cv2.COLOR_BGR2Lab)

        # make default black/red/bar image
        if self.is_image:
            img_black = np.zeros(shape=self.img_size, dtype=np.uint8)
            img_red = np.zeros(shape=self.img_size, dtype=np.uint8)
        img_bar = np.zeros(shape=self.img_size, dtype=np.uint8)

        # get black/red/bar image by using threshold
        if self.is_image:
            img_black[(abs(img_lab[:,:,1])<self.thresh_black) & (abs(img_lab[:,:,2])<self.thresh_black) & \
                        (img_lab[:,:,0]<85)] = 255
            img_red[img_lab[:,:,1]>self.thresh_red] = 255
        img_bar[((abs(img_lab[:,:,1])>self.thresh_black) | (abs(img_lab[:,:,2])>self.thresh_black) | (img_lab[:,:,0]>85)) \
                & (img_lab[:,:,1]<self.thresh_red) & (img_lab[:,:,0]>30)] = 255
        
        # blurring to remove noise
        if self.is_image:
            img_black = cv2.medianBlur(img_black, 3)
            img_red = cv2.medianBlur(img_red, 3)

        # apply morphological transformations
        if self.prev_rect[2] < self.width*0.3:
            kernel = np.ones((3,3),np.uint8)
        else:
            kernel = np.ones((5,5),np.uint8)
        img_bar = cv2.morphologyEx(img_bar, cv2.MORPH_OPEN, kernel)

        # get countour info
        _, contours, _ = cv2.findContours(img_bar, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

        pos_x = img_bar.shape[1]
        get_reward = False
        cur_rect = (0,0,0,0)
        # if there's no blob
        if len(contours) == 0 or (len(contours)==1 and cv2.contourArea(contours[0])<self.min_blob_area):
            # no counter buffer
            self.count_no_contour += 1
            if self.count_no_contour >= 10:
                get_reward = True
                self.prev_rect = (self.width-1, self.height-1, 0, 0)
                reward = self.prev_rect[2] - w_prev
                return 'get', self.prev_rect, reward
        else:
            self.count_no_contour = 0

            for cnt in contours:
                area = cv2.contourArea(cnt)
                x,y,w,h = cv2.boundingRect(cnt)

                # check blob area and position
                # area is greater than min_blob_area & get most left blob & search area is the region of previous rectangle
                if area>self.min_blob_area and x<pos_x and x>self.prev_rect[0]-self.width*0.02:
                    pos_x = x

                    # adjust postion of left edge
                    for i in range(x,x+w):
                        white_count = 0
                        for j in range(img_bar.shape[0]):
                            if img_bar[j,x] == 255:
                                white_count += 1
                        if white_count<(img_bar.shape[0]/2):
                            x += 1
                            w -= 1
                        else:
                            break

                    # skip noise blob (zero width)
                    if x>self.width*0.01 and w==0:
                        continue

                    # check left edge of blob
                    last_x = pos_x
                    for i in range(pos_x,img_bar.shape[1]):
                        white_count = 0
                        for j in range(img_bar.shape[0]):
                            if img_bar[j,i] == 255:
                                white_count += 1
                        # 'PLAYER #' region
                        if x<self.width*0.15:
                            criteria = int(img_bar.shape[0]*0.9)
                        else:
                            criteria = img_bar.shape[0]
                        if white_count >= criteria:
                            last_x = i
                            break

                    dist = last_x - pos_x
                    w = img_bar.shape[1] - x
                    # 'PLAYER #' region
                    if x<self.width*0.15:
                        dist_crit = self.width*0.02
                    else:
                        dist_crit = self.width*0.01

                    if dist <= dist_crit:
                        get_reward = True
                        cur_rect = (x, y, w, h)

        if get_reward:
            state = 'get'
        else:
            state = 'block'

        if self.is_image:
            return state, cur_rect, 0, (img_black, img_red, img_bar)
        else:
            reward = 0
            if state == 'get':
                self.w_hist.append(cur_rect[2])
                # if all values in w_hist are equl
                if all(self.w_hist[0] == item for item in self.w_hist):
                    reward = cur_rect[2] - self.prev_rect[2]
                    self.prev_rect = cur_rect
                    
            return state, self.prev_rect, reward
    
    # apply adaptive threshold
    def update_thresh(self):
        if self.prev_rect[2]<self.width*0.35:
            self.thresh_red = 40
        else:
            self.thresh_red = 20

    def set_prev_rect(self, x, y, width, height):
        self.prev_rect = (x, y, width, height)

    def reset_reader(self):
        self.prev_rect = (0, 0, self.width, self.height)
        self.count_no_contour = 0
        for i in range(5):
            self.w_hist.append(self.width)
