import numpy as np
import cv2
import glob

from rewardreader import RewardReader

filenames = glob.glob('**/image_2422_r.bmp')
# filenames = glob.glob('/Users/nautilus22/DeepLearning/source_code/git_codes/sfv_img_proc/issue/image_*.bmp')
filenames.sort()

reader = RewardReader(22, 730, is_image=True)
x = 460
reader.set_prev_rect(x, 0, 730-x, 22)

for name in filenames:
    image = cv2.imread(name)

    status, (x,y,w,h), reward, (black, red, bar) = reader.get_bound_rect(image)

    image_rect = np.copy(image)
    font = cv2.FONT_HERSHEY_SIMPLEX
    if status == 'get':
        # draw rectangle and show width info
        image_rect = cv2.rectangle(image_rect,(x,y),(x+w,y+h),(255,0,0),1)
    else:
        cv2.putText(image_rect,'Block',(400,15), font, 0.5,(0,0,255),2,cv2.LINE_AA)
    cv2.putText(image_rect,'w='+str(w),(670,15), font, 0.5,(255,255,255),1,cv2.LINE_AA)
    # display reward
    cv2.putText(image_rect,'r='+str(reward),(120,15), font, 0.5,(255,255,255),1,cv2.LINE_AA)

    cv2.imshow(name, image)
    cv2.imshow(name+'_result', image_rect)
    cv2.imshow(name+'_remain', bar)
    cv2.imshow(name+'_black', black)
    cv2.imshow(name+'_red', red)
    
    cv2.moveWindow(name, 500, 100)
    cv2.moveWindow(name+'_result', 500, 250)
    cv2.moveWindow(name+'_remain', 500, 400)
    cv2.moveWindow(name+'_black', 500, 550)
    cv2.moveWindow(name+'_red', 500, 700)

    quit_main = False
    while True:
        key = cv2.waitKey(0)
        if key>0:
            if key & 0xFF == ord('q'):
                quit_main = True
            cv2.destroyAllWindows()
            break
            
    if quit_main:
        break